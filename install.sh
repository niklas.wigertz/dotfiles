#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Install Brew
if ! [ -x "$(which brew)" ]; then
  echo "installing brew"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> $HOME/.zprofile
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# Install apps
echo "installing apps"
brew bundle --file $SCRIPT_DIR/assets/Brewfile
mkdir -p ~/.nvm

# Change default shell
ZSH_PATH=$(brew --prefix)/bin/zsh
CURRENT_SHELL=$(dscl . -read ~/ UserShell | sed 's/UserShell: //')
if [ "$ZSH_PATH" != "$CURRENT_SHELL" ]; then
  echo "setting default shell"
  sudo dscl . -create /Users/$USER UserShell $ZSH_PATH
  exec $ZSH_PATH
else
  echo "default shell already set"
fi

# Install oh-my-zsh
if [ ! -d ~/.oh-my-zsh ]; then
  echo "installing oh-my-zsh"
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
else
  echo "oh-my-zsh is already installed"
fi

# Setup dotfiles
rcup -v \
  -d $SCRIPT_DIR/rcm \
  -U Library

# Install node
/opt/homebrew/bin/zsh -c "source ~/.zshrc && nvm install --lts"
