#!/usr/bin/env bash

if [ "$AWS_PROFILE" == "" ]; then
  echo ""
  exit 0
fi

PROMPT_DIR=~/.aws/.prompt
CREDENTIALS_SHA_PATH=$PROMPT_DIR/.credentials.sha1
CONFIG_SHA_PATH=$PROMPT_DIR/.config.sha1
PROFILE_SELECTED_PATH=$PROMPT_DIR/.profile
OUTPUT_PATH=$PROMPT_DIR/.output

mkdir -p $PROMPT_DIR

CURRENT_CREDENTIALS_SHA=$(shasum ~/.aws/credentials | cut -f1 -d ' ')
CURRENT_CONFIG_SHA=$(shasum ~/.aws/config | cut -f1 -d ' ')

if [ -f "$CREDENTIALS_SHA_PATH" ]; then
  LAST_CREDENTIALS_SHA=$(cat $CREDENTIALS_SHA_PATH)
fi
if [ -f "$CONFIG_SHA_PATH" ]; then
  LAST_CONFIG_SHA=$(cat $CONFIG_SHA_PATH)
fi
if [ -f "$PROFILE_SELECTED_PATH" ]; then
  LAST_PROFILE_SELECTED=$(cat $PROFILE_SELECTED_PATH)
fi
if [ -f "$OUTPUT_PATH" ]; then
  LAST_OUTPUT=$(cat $OUTPUT_PATH)
fi

printf $CURRENT_CREDENTIALS_SHA > $CREDENTIALS_SHA_PATH
printf $CURRENT_CONFIG_SHA > $CONFIG_SHA_PATH
printf $AWS_PROFILE > $PROFILE_SELECTED_PATH

update=false
if [[ "$LAST_CREDENTIALS_SHA" != "$CURRENT_CREDENTIALS_SHA" ]]; then update=true; fi
if [[ "$LAST_CONFIG_SHA" != "$CURRENT_CONFIG_SHA" ]]; then update=true; fi
if [[ "$LAST_PROFILE_SELECTED" != "$AWS_PROFILE" ]]; then update=true; fi
if [[ "$LAST_OUTPUT" == "" ]]; then update=true; fi

if [[ "$update" == false ]]; then 
  echo "$LAST_OUTPUT"
  exit 0
fi

role=$(aws --profile $AWS_PROFILE configure get role_arn) || true
source_profile=$(aws --profile $AWS_PROFILE configure get source_profile) || true
region=$(aws --profile $AWS_PROFILE configure get region) || true
if [[ "$source_profile" != "" && "$region" == "" ]]; then 
  region=$(aws --profile $source_profile configure get region) || true
fi
if [[ "$role" != "" ]]; then 
  role_name=$(printf "$role" | awk -F '/' '{print $2}' || true)
  output="${AWS_PROFILE}/$region/$role_name"
else
  output="${AWS_PROFILE}/$region"
fi

printf "$output" > $OUTPUT_PATH

echo "$output"
