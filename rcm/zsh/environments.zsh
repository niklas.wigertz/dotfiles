valid_envs=(
  common
  development 
  production
  qa
  staging
)

function contains_element() {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

function environment() {
  reload #fix: workaround for missing "asp" command of first run
  target_env=$1
  echo "Using $target_env"

  contains_element "$target_env" "${valid_envs[@]}"
  if [ $? -ne 0 ]; then
    echo "invalid environment '$target_env'"
    echo "valid enironments: $valid_envs"
  else
    asp $target_env

    source_profile=$(aws --profile $target_env configure get source_profile) || true
    region=$(aws --profile $target_env configure get region) || true
    if [[ "$source_profile" != "" && "$region" == "" ]]; then 
      region=$(aws --profile $source_profile configure get region) || true
    fi

    aws eks update-kubeconfig --region $region --profile $target_env --name $target_env --alias "eks:$target_env"
  fi
}

function _environment() {
  compadd -- $valid_envs
}

compdef _environment environment