# Dotfiles

# Commands

## environment
Switch environment with the `environment` command.

Currently sets `AWS_PROFILE` and `EKS` cluster.

Source: `<REPO>/rcm/zsh/environments.zsh`

```
environment <NAME>

#eg.
environment development
```


## Links
https://saiankit30.medium.com/how-to-change-the-integrated-terminal-in-vs-code-from-default-to-pro-iterm-ish-5c958e13aada

https://dev.to/rossijonas/how-to-set-up-history-based-autocompletion-in-zsh-k7o

https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md

Faster Dock
https://apple.stackexchange.com/a/34097