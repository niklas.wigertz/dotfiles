# Convenience
alias reload='source ~/.zshrc'

# Git
alias gg="git pull --rebase --autostash"

# Environments
alias ec="environment common"
alias ed="environment development"
alias ep="environment production"
alias eq="environment qa"
alias es="environment staging"
